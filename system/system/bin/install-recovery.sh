#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:09dbaf670aa25d0b88f1dbd64a1e13f66c030ded; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:d20c92197f5dc75c2a932f5c1807a7faa45b190a \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:09dbaf670aa25d0b88f1dbd64a1e13f66c030ded && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
