#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),LD_C15_TFT_LFLT)
include $(call all-subdir-makefiles,$(LOCAL_PATH))
endif
