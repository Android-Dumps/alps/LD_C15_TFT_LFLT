#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_LD_C15_TFT_LFLT.mk

COMMON_LUNCH_CHOICES := \
    lineage_LD_C15_TFT_LFLT-user \
    lineage_LD_C15_TFT_LFLT-userdebug \
    lineage_LD_C15_TFT_LFLT-eng
